import java.util.ArrayList;
import java.util.List;

import ro.pub.cs.lcpl.*;

public class NoCircularInheritance extends ParentExists {
	public NoCircularInheritance(Program p) throws Exception {
		super(p);
		
		if (program.getClasses() != null) {
			for (LCPLClass cls : program.getClasses()) {
				List<String> names = new ArrayList<String>();
				for (LCPLClass parent = cls; parent != null; parent = parent.getParentData()) {
					String name = parent.getName();
					if (names.contains(name)) {
						throw new LCPLException(parent.getLineNumber(), "Class " + name +
												" recursively inherits itself.");
					}
					names.add(name);
				}
			}
		}
	}
}
