import java.util.HashMap;
import java.util.Map;

import ro.pub.cs.lcpl.*;

class LCPLException extends Exception {

	public LCPLException(int line, String err) {
		super("Error in line " + line + " : " + err);
	}
}

public class SemanticChecker {
	protected Program program;
	protected Map<String, LCPLClass> classesTable;
	
	public SemanticChecker(Program p) {
		program = p;
		
		BasicTypes bt = new BasicTypes();
		
		program.setIntType(bt.getIntType());
		program.setNoType(bt.getNoType());
		program.setNullType(bt.getNullType());
		program.setObjectType(bt.getObjectType());
		program.setStringType(bt.getStringType());
		program.setIoType(bt.getIOType());
		
		classesTable = new HashMap<String, LCPLClass>();
		classesTable.put("Object", program.getObjectType());
		classesTable.put("String", program.getStringType());
		classesTable.put("IO", program.getIoType());
	}
	
	public Program getProgram() {
		return program;
	}
	
	public Type getTypeByName(String name) {
		Type type = null;
		if (name != null) {
			if (name.equals("void")) {
				type = program.getNoType();
			} else if (name.equals("Int")) {
				type = program.getIntType();
			} else {
				type = classesTable.get(name);
			}
		}
		return type;
	}
}
