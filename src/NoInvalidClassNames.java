import java.util.ArrayList;
import java.util.List;

import ro.pub.cs.lcpl.*;

class RedefinedBasicTypeException extends LCPLException {
	public RedefinedBasicTypeException(int line, String cls) {
		super(line, "Cannot redefine basic type " + cls);
	}
}

public class NoInvalidClassNames extends SemanticChecker {

	public NoInvalidClassNames(Program p) throws Exception {
		super(p);
		
		List<LCPLClass> classes = program.getClasses();
		List<String> basic = new ArrayList<String>();
		basic.add("Int");
		basic.add("String");
		basic.add("IO");
		basic.add("Object");
		
		for (int i = 0; classes != null && i < classes.size(); i++) {
			String name = classes.get(i).getName();
			int line = classes.get(i).getLineNumber();
			if (basic.contains(name)) {
				throw new RedefinedBasicTypeException(line, name);
			}
			for (int j = 0; j < i; j++) {
				String name2 = classes.get(j).getName();
				if (name.equals(name2)) {
					throw new LCPLException(line, "A class with the same name already exists : " + name);
				}
			}
			classesTable.put(name, classes.get(i));
		}
	}
}
