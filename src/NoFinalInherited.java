import java.util.ArrayList;
import java.util.List;

import ro.pub.cs.lcpl.*;


public class NoFinalInherited extends NoInvalidClassNames {
	public NoFinalInherited(Program p) throws Exception {
		super(p);
		
		if (program.getClasses() != null) {
			for (LCPLClass cls : program.getClasses()) {
				String parent = cls.getParent();
				if (parent != null) {
					if (parent.equals(program.getIntType().getName())) {
						throw new LCPLException(cls.getLineNumber(), "Class " + parent + " not found.");
					} else if (parent.equals(program.getStringType().getName())) {
						throw new LCPLException(cls.getLineNumber(), "A class cannot inherit a " +
																		program.getStringType().getName());
					}
				}
			}
		}
	}

}
