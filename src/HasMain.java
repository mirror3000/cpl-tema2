import ro.pub.cs.lcpl.*;

public class HasMain extends NoDuplicateFormalParameters {
	public HasMain(Program p) throws Exception {
		super(p);
		
		if (program.getClasses() != null) {
			for (LCPLClass cls : program.getClasses()) {
				if (cls.getName().equals("Main")) {
					for (Method method : allFeatures.get(cls.getName()).getLeft()) {
						if (method.getName().equals("main")) {
							return;
						}
					}
					throw new LCPLException(cls.getLineNumber(), "Method main not found in class Main");
				}
			}
		}
		throw new LCPLException(program.getLineNumber(), "Class Main not found.");
	}
}
