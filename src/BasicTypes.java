import java.util.ArrayList;

import ro.pub.cs.lcpl.*;

public class BasicTypes {
	private IntType intType;
	private NoType noType;
	private NullType nullType;
	private LCPLClass objectType;
	private LCPLClass stringType;
	private LCPLClass ioType;
	
	private IntType initIntType() {
		return new IntType();
	}
	
	private NoType initNoType() {
		return new NoType();
	}
	
	private NullType initNullType() {
		return new NullType();
	}
	
	private LCPLClass initObjectType() {
		return new LCPLClass(0, "Object", null, new ArrayList<Feature>());
	}
	
	private LCPLClass initStringType() {
		LCPLClass string = new LCPLClass(0, "String", "Object", new ArrayList<Feature>());
		string.setParentData(objectType);
		return string;
	}
	
	private LCPLClass initIOType() {
		LCPLClass io = new LCPLClass(0, "IO", "Object", new ArrayList<Feature>());
		io.setParentData(objectType);
		return io;
	}
	
	private void initObjectMethods() {
		FormalParam self;
		
		Method abort = new Method(0, "abort", new ArrayList<FormalParam>(), "void", null);
		abort.setParent(objectType);
		self = new FormalParam("self", objectType.getName());
		self.setVariableType(objectType);
		abort.setSelf(self);
		abort.setReturnTypeData(noType);
		objectType.getFeatures().add(abort);
		
		Method typeName = new Method(0, "typeName", new ArrayList<FormalParam>(), "String", null);
		typeName.setParent(objectType);
		self = new FormalParam("self", objectType.getName());
		self.setVariableType(objectType);
		typeName.setSelf(self);
		typeName.setReturnTypeData(stringType);
		objectType.getFeatures().add(typeName);
		
		Method copy = new Method(0, "copy", new ArrayList<FormalParam>(), "Object", null);
		copy.setParent(objectType);
		self = new FormalParam("self", objectType.getName());
		self.setVariableType(objectType);
		copy.setSelf(self);
		copy.setReturnTypeData(objectType);
		objectType.getFeatures().add(copy);
	}
	
	private void initStringMethods() {
		FormalParam self;
		
		Method length = new Method(0, "length", new ArrayList<FormalParam>(), "Int", null);
		length.setParent(stringType);
		self = new FormalParam("self", stringType.getName());
		self.setVariableType(stringType);
		length.setSelf(self);
		length.setReturnTypeData(intType);
		stringType.getFeatures().add(length);
		
		Method toInt = new Method(0, "toInt", new ArrayList<FormalParam>(), "Int", null);
		toInt.setParent(stringType);
		self = new FormalParam("self", stringType.getName());
		self.setVariableType(stringType);
		toInt.setSelf(self);
		toInt.setReturnTypeData(intType);
		stringType.getFeatures().add(toInt);
	}
	
	private void initIOMethods() {
		FormalParam self;
		
		Method out = new Method(0, "out", new ArrayList<FormalParam>(), "IO", null);
		out.setParent(ioType);
		self = new FormalParam("self", ioType.getName());
		self.setVariableType(ioType);
		out.setSelf(self);
		out.setReturnTypeData(ioType);
		FormalParam msg = new FormalParam("msg", "String");
		msg.setVariableType(stringType);
		out.getParameters().add(msg);
		ioType.getFeatures().add(out);
		
		Method in = new Method(0, "in", new ArrayList<FormalParam>(), "String", null);
		in.setParent(ioType);
		self = new FormalParam("self", ioType.getName());
		self.setVariableType(ioType);
		in.setSelf(self);
		in.setReturnTypeData(stringType);
		ioType.getFeatures().add(in);
	}
	
	public BasicTypes() {
		intType = initIntType();
		noType = initNoType();
		nullType = initNullType();
		objectType = initObjectType();
		stringType = initStringType();
		ioType = initIOType();
		
		initObjectMethods();
		initStringMethods();
		initIOMethods();
	}
	
	public IntType getIntType() {
		return intType;
	}
	
	public NoType getNoType() {
		return noType;
	}
	
	public NullType getNullType() {
		return nullType;
	}
	
	public LCPLClass getObjectType() {
		return objectType;
	}
	
	public LCPLClass getStringType() {
		return stringType;
	}
	
	public LCPLClass getIOType() {
		return ioType;
	}
}
