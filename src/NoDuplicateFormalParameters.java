import java.util.List;

import ro.pub.cs.lcpl.*;

class FormalParametersDuplicateException extends LCPLException {
	public FormalParametersDuplicateException(int line, int line2, String param) {
		super(line, "Parameter name " + param + " already used at " + line2);
	}
}

public class NoDuplicateFormalParameters extends NoDuplicateFeatures {
	public NoDuplicateFormalParameters(Program p) throws Exception {
		super(p);
		
		if (program.getClasses() != null) {
			for (LCPLClass cls : program.getClasses()) {
				if (cls.getFeatures() != null) {
					for (Feature feature : cls.getFeatures()) {
						if (feature instanceof Method) {
							Method mtd = (Method)feature;
							List<FormalParam> params = mtd.getParameters();
							for (int i = 0; params != null && i < params.size(); i++) {
								int line = params.get(i).getLineNumber();
								String name = params.get(i).getName();
								for (int j = 0; j < i; j++) {
									int line2 = params.get(j).getLineNumber();
									String name2 = params.get(j).getName();
									if (name2.equals(name)) {
										throw new FormalParametersDuplicateException(line, line2, name);
									}
								}
							}
						}
					}
				}
			}
		}
	}
}
