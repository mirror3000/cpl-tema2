import java.util.ArrayList;
import java.util.List;

import ro.pub.cs.lcpl.*;

public class ParentExists extends NoFinalInherited {
	public ParentExists(Program p) throws Exception {
		super(p);
		
		if (program.getClasses() != null) {
			for (LCPLClass cls : program.getClasses()) {
				String parentName = cls.getParent();
				if (parentName != null) {
					if (classesTable.containsKey(parentName)) {
						cls.setParentData(classesTable.get(parentName));
					} else {
						throw new LCPLException(cls.getLineNumber(), "Class " + parentName + " not found.");
					}
				} else {
					cls.setParentData(program.getObjectType());
					cls.setParent(program.getObjectType().getName());
				}
			}
		}
	}
}
