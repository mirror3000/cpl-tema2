import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ro.pub.cs.lcpl.*;

public class NoDuplicateFeatures extends NoCircularInheritance {
	
	protected Map<String, Pair<List<Method>, List<Attribute>>> allFeatures;
	
	public NoDuplicateFeatures(Program p) throws Exception {
		super(p);
		
		allFeatures = new HashMap<String, Pair<List<Method>, List<Attribute>>>();
		
		if (program.getClasses() != null) {
			for (LCPLClass cls : program.getClasses()) {
				allFeatures.put(cls.getName(), getAllFeatures(cls));
			}
		}
		allFeatures.put(program.getObjectType().getName(), getAllFeatures(program.getObjectType()));
		allFeatures.put(program.getStringType().getName(), getAllFeatures(program.getStringType()));
		allFeatures.put(program.getIoType().getName(), getAllFeatures(program.getIoType()));
	}
	
	private Pair<List<Method>, List<Attribute>> getAllFeatures(LCPLClass cls) throws Exception {
		List<Method> methods = new ArrayList<Method>();
		List<Attribute> attrsOnly = new ArrayList<Attribute>();
		List<Pair<LCPLClass, Attribute>> attributes = new ArrayList<Pair<LCPLClass, Attribute>>();
		
		if (cls.getFeatures() != null) {
			for (Feature feature : cls.getFeatures()) {
				if (feature instanceof Method) {
					methods.add((Method)feature);
				} else if (feature instanceof Attribute) {
					attributes.add(new Pair<LCPLClass, Attribute>(cls, (Attribute)feature));
				}
			}
		}
		
		for (int i = 0; i < methods.size(); i++) {
			int line = methods.get(i).getLineNumber();
			String name = methods.get(i).getName();
			for (int j = 0; j < i; j++) {
				String name2 = methods.get(j).getName();
				if (name.equals(name2)) {
					throw new LCPLException(line, "A method with the same name already exists in class " +
													cls.getName() + " : " + name);
				}
			}
		}
		
		for (int i = 0; i < attributes.size(); i++) {
			int line = attributes.get(i).getRight().getLineNumber();
			String name = attributes.get(i).getRight().getName();
			for (int j = 0; j < i; j++) {
				String name2 = attributes.get(j).getRight().getName();
				if (name.equals(name2)) {
					throw new LCPLException(line, "An attribute with the same name already exists in class " +
													cls.getName() + " : " + name);
				}
			}
		}
			
		for (cls = cls.getParentData(); cls != null; cls = cls.getParentData()) {
			List<Method> localMethods = new ArrayList<Method>();
			List<Method> addableMethods = new ArrayList<Method>();
			
			if (cls.getFeatures() != null) {
				for (Feature feature : cls.getFeatures()) {
					if (feature instanceof Method) {
						localMethods.add((Method)feature);
					} else if (feature instanceof Attribute) {
						attributes.add(new Pair<LCPLClass, Attribute>(cls, (Attribute)feature));
					}
				}
			}
			
			for (int i = 0; i < localMethods.size(); i++) {
				String name = localMethods.get(i).getName();
				boolean addable = true;
				
				for (int j = 0; j < methods.size(); j++) {
					if (name.equals(methods.get(j).getName())) {
						addable = false;
						checkSignature(localMethods.get(i), methods.get(j));
						break;
					}
				}
				if (addable) {
					addableMethods.add(localMethods.get(i));
				}
			}
			
			methods.addAll(addableMethods);
		}
		
		for (int i = attributes.size() - 1; i >= 0; i--) {
			String name = attributes.get(i).getRight().getName();
			for (int j = i - 1; j >= 0; j--) {
				String name2 = attributes.get(j).getRight().getName();
				if (name.equals(name2)) {
					throw new LCPLException(attributes.get(i).getLeft().getLineNumber(), "Attribute " + name +
																							" is redefined.");
				}
			}
		}
		
		for (Pair<LCPLClass, Attribute> pair : attributes) {
			attrsOnly.add(pair.getRight());
		}
		return new Pair<List<Method>, List<Attribute>>(methods, attrsOnly);
	}
	
	private void checkSignature(Method m1, Method m2) throws Exception {
		if (m1 == null || m2 == null) {
			return;
		}
		if (!m1.getName().equals(m2.getName())) {
			return;
		}
		if (!m1.getReturnType().equals(m2.getReturnType())) {
			throw new LCPLException(m2.getLineNumber(), "Return type changed in overloaded method.");
		}
		List<FormalParam> params1 = m1.getParameters();
		List<FormalParam> params2 = m2.getParameters();
		
		if ((params1 == null) != (params2 == null) || params1.size() != params2.size()) {
			throw new LCPLException(m2.getLineNumber(), "Overloaded method has a different number of parameters");
		}
		
		for (int i = 0; i < params1.size(); i++) {
			if (!params1.get(i).getType().equals(params2.get(i).getType())) {
				throw new LCPLException(m2.getLineNumber(), "Parameter " + params2.get(i).getName() +
															" has a different type in overloaded method.");
			}
		}
	}
}
