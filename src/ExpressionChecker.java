import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import ro.pub.cs.lcpl.*;

class ExpressionInvalidTypesException extends LCPLException {
	public ExpressionInvalidTypesException(int line, String exprName, String types) {
		super(line, "Wrong types. Expression " + exprName + " expects: " + types);
	}
}

public class ExpressionChecker extends SetFeatureTypesAndSelf {
	public ExpressionChecker(Program p) throws Exception {
		super(p);
		
		if (program.getClasses() != null) {
			for (LCPLClass cls : program.getClasses()) {
				List<Variable> attributes = new ArrayList<Variable>(allFeatures.get(cls.getName()).getRight());
				if (cls.getFeatures() != null) {
					for (Feature feature : cls.getFeatures()) {
						if (feature instanceof Method) {
							Method mtd = (Method)feature;
							List<Variable> varsInScope = new ArrayList<Variable>(attributes);
							
							if (mtd.getParameters() != null) {
								varsInScope.addAll(mtd.getParameters());
							}
							varsInScope.add(mtd.getSelf());
							
							setExprData(mtd.getBody(), varsInScope);
							
							if (mtd.getReturnTypeData() != program.getNoType()) {
								Expression body = mtd.getBody();
								Type retType = mtd.getReturnTypeData();
								if (body == null) {
									throw new LCPLException(mtd.getLineNumber(), "Cannot convert a value of type " +
															program.getNoType().getName() + " into " +
															retType.getName());
								}else if (body.getTypeData() != retType) {
									if (validCast(retType, body.getTypeData())) {
										Cast cast = new Cast(body.getLineNumber(), retType.getName(), body);
										cast.setTypeData(retType);
										mtd.setBody(cast);
									} else {
										throw new LCPLException(mtd.getBody().getLineNumber(),
																"Cannot convert a value of type " +
																mtd.getBody().getType() + " into " +
																mtd.getReturnType());
									}
								}
							}
						} else if (feature instanceof Attribute) {
							Attribute attr = (Attribute)feature;
							Expression init = attr.getInit();
							List<Variable> varsInScope = new ArrayList<Variable>(attributes);
							
							varsInScope.add(attr.getAttrInitSelf());
							
							setExprData(init, varsInScope);
							
							if (init != null && init.getTypeData() != attr.getTypeData()) {
								if (validCast(attr.getTypeData(), init.getTypeData())) {
									Cast cast = new Cast(init.getLineNumber(), attr.getType(), init);
									cast.setTypeData(attr.getTypeData());
									attr.setInit(cast);
								} else {
									throw new LCPLException(attr.getLineNumber(),
															"Cannot convert a value of type " +
															attr.getType() + " into " + attr.getInit().getType());
									
								}
							}
						}
					}
				}
			}
		}
	}
	
	private LCPLClass getLastCommonAncestor(LCPLClass c1, LCPLClass c2) {
		Stack<LCPLClass> ancC1 = new Stack<LCPLClass>();
		Stack<LCPLClass> ancC2 = new Stack<LCPLClass>();
		LCPLClass lastCommon = null;
		
		for (; c1 != null; c1 = c1.getParentData()) {
			ancC1.push(c1);
		}
		for (; c2 != null; c2 = c2.getParentData()) {
			ancC2.push(c2);
		}
		
		for (;!ancC1.empty() && !ancC2.empty() && ancC1.peek() == ancC2.peek();
				ancC1.pop(), ancC2.pop()) {
			lastCommon = ancC1.peek();
		}
		
		return lastCommon;
	}
	
	private void splitVarList(List<Variable> all, List<LocalDefinition> locals, List<FormalParam> parameters,
								List<Attribute> attributes) {
		for (Variable var : all) {
			if (var instanceof LocalDefinition) {
				locals.add((LocalDefinition)var);
			} else if (var instanceof FormalParam) {
				parameters.add((FormalParam)var);
			} else if (var instanceof Attribute) {
				attributes.add((Attribute)var);
			}
		}
	}
	
	private Variable getVarByName(List<Variable> varsInScope, String name) {
		Variable foundVar = null;
		
		if (name.startsWith("self.")) {
			String nameShort = name.substring("self.".length());
			for (Variable var : varsInScope) {
				if (var instanceof Attribute) {
					if (((Attribute)var).getName().equals(nameShort)) {
						foundVar = var;
						break;
					}
				}
			}
		} else {
			List<LocalDefinition> locals = new ArrayList<LocalDefinition>();
			List<FormalParam> parameters = new ArrayList<FormalParam>();
			List<Attribute> attributes = new ArrayList<Attribute>();
			
			splitVarList(varsInScope, locals, parameters, attributes);
			
			for (LocalDefinition ldef : locals) {
				if (ldef.getName().equals(name)) {
					foundVar = ldef;
					break;
				}
			}
			if (foundVar == null) {
				for (FormalParam param : parameters) {
					if (param.getName().equals(name)) {
						foundVar = param;
						break;
					}
				}
			}
			if (foundVar == null) {
				for (Attribute attr : attributes) {
					if (attr.getName().equals(name)) {
						foundVar = attr;
						break;
					}
				}
			}
		}
		
		return foundVar;
	}
	
	private Type getVarType(Variable var) {
		if (var instanceof LocalDefinition) {
			return ((LocalDefinition)var).getVariableType();
		} else if (var instanceof FormalParam) {
			return ((FormalParam)var).getVariableType();
		} else if (var instanceof Attribute) {
			return ((Attribute)var).getTypeData();
		} else {
			return null;
		}
	}
	
	private boolean validCast(Type base, Type toCast) {
		if (base == toCast) {
			return true;
		} else if (base == program.getStringType() &&
					toCast == program.getIntType()) {
			return true;
		} else if ((base instanceof LCPLClass) && (toCast instanceof LCPLClass)) {
			LCPLClass toCastCls = (LCPLClass)toCast;
			for (; toCastCls != null && toCastCls != base;
					toCastCls = toCastCls.getParentData());
			return toCastCls != null;
		} else if ((base instanceof LCPLClass) && (toCast instanceof NullType)) {
			return true;
		} else {
			return false;
		}
	}
	
	private void computeDispatch(BaseDispatch dispatch, List<Variable> varsInScope) throws Exception {
		List<FormalParam> formals = dispatch.getMethod().getParameters();
		List<Expression> actuals = dispatch.getArguments();
		
		if (formals.size() < actuals.size()) {
			throw new LCPLException(dispatch.getLineNumber(), "Too many arguments in method call " +
																dispatch.getName());
		} else if (formals.size() > actuals.size()) {
			throw new LCPLException(dispatch.getLineNumber(), "Not enough arguments in method call " +
																dispatch.getName());
		}
		
		for (int i = 0; i < actuals.size(); i++) {
			Expression ex = actuals.get(i);
			FormalParam fp = formals.get(i);
			
			if (ex == null) {
				throw new LCPLException(dispatch.getLineNumber(), "Cannot convert a value of type " +
																	program.getNoType().getName() + " into " +
																	fp.getVariableType().getName());
			}
			
			setExprData(ex, varsInScope);
			
			if (ex.getTypeData() != fp.getVariableType()) {
				if (validCast(fp.getVariableType(), ex.getTypeData())) {
					Cast cast = new Cast(ex.getLineNumber(), fp.getType(), ex);
					cast.setTypeData(fp.getVariableType());
					actuals.set(i, cast);
				} else {
					throw new LCPLException(dispatch.getLineNumber(), "Cannot convert a value of type " +
																		ex.getType() + " into " +
																		fp.getVariableType().getName()); 
				}
			}
		}
		dispatch.setTypeData(dispatch.getMethod().getReturnTypeData());
		dispatch.setType(dispatch.getTypeData().getName());
	}
	
	private void setExprData(Expression expr, List<Variable> varsInScope) throws Exception {
		if (expr == null) {
			//DO NOTHING
		} else if (expr instanceof IntConstant) {
			expr.setTypeData(program.getIntType());
		} else if (expr instanceof StringConstant) {
			expr.setTypeData(program.getStringType());
		} else if (expr instanceof VoidConstant) {
			expr.setTypeData(program.getNullType());
		} else if (expr instanceof Symbol) {
			Symbol sym = (Symbol)expr;
			Variable foundVar = getVarByName(varsInScope, sym.getName());
			
			if (foundVar == null) {
				throw new LCPLException(expr.getLineNumber(), "Attribute " + sym.getName().replace("self.", "") +
																" not found in class " +
																((FormalParam)getVarByName(varsInScope, "self")).getType());
			}
			sym.setVariable(foundVar);
			
			expr.setTypeData(getVarType(foundVar));
		} else if (expr instanceof UnaryOp) {
			UnaryOp unaryOp = (UnaryOp)expr;
			Expression ex = unaryOp.getE1();

			if (ex == null) {
				throw new ExpressionInvalidTypesException(expr.getLineNumber(), "unary operation", "Int");
			}
			
			setExprData(ex, varsInScope);
			
			if (ex.getTypeData() != program.getIntType()) {
				throw new ExpressionInvalidTypesException(expr.getLineNumber(), "unary operation", "Int");
			}
			
			expr.setTypeData(program.getIntType());
		} else if (expr instanceof Addition) {
			Addition addition = (Addition)expr;
			Expression ex1 = addition.getE1();
			Expression ex2 = addition.getE2();
			
			if (ex1 == null || ex2 == null) {
				throw new LCPLException(expr.getLineNumber(), "Cannot convert '+' expression to Int or String");
			}
			
			setExprData(ex1, varsInScope);
			setExprData(ex2, varsInScope);
			
			if (ex1.getTypeData() == ex2.getTypeData()) {
				if (ex1.getTypeData() == program.getIntType()) {
					expr.setTypeData(program.getIntType());
				} else if (ex1.getTypeData() == program.getStringType()) {
					expr.setTypeData(program.getStringType());
				} else {
					throw new LCPLException(expr.getLineNumber(), "Cannot convert '+' expression to Int or String");
				}
			} else {
				if (ex1.getTypeData() == program.getIntType() && ex2.getTypeData() == program.getStringType()) {
					Cast cast = new Cast(ex1.getLineNumber(), "String", ex1);
					cast.setTypeData(program.getStringType());
					addition.setE1(cast);
				} else if (ex1.getTypeData() == program.getStringType() && ex2.getTypeData() == program.getIntType()) {
					Cast cast = new Cast(ex2.getLineNumber(), "String", ex2);
					cast.setTypeData(program.getStringType());
					addition.setE2(cast);
				} else {
					if (ex1.getTypeData() == program.getStringType() || ex2.getTypeData() == program.getStringType()) {
						String type = ex1.getTypeData() != program.getStringType() ? ex1.getType() : ex2.getType();
						//Having two different message types for '+' does not make much sense, but if the tests require it...
						throw new LCPLException(expr.getLineNumber(), "Cannot convert a value of type " + type +
																		" into String");
					} else {
						throw new LCPLException(expr.getLineNumber(), "Cannot convert '+' expression to Int or String");
					}
				}
				
				expr.setTypeData(program.getStringType());
			}
		} else if (expr instanceof EqualComparison) {
			EqualComparison eqCmp = (EqualComparison)expr;
			Expression ex1 = eqCmp.getE1();
			Expression ex2 = eqCmp.getE2();
			
			if (ex1 == null || ex2 == null) {
				throw new LCPLException(expr.getLineNumber(), "Invalid type of parameters for == expression");
			}
			
			setExprData(ex1, varsInScope);
			setExprData(ex2, varsInScope);
			
			if (ex1.getTypeData() == program.getNoType() || ex2.getTypeData() == program.getNoType()) {
				throw new LCPLException(expr.getLineNumber(), "Invalid type of parameters for == expression");
			} else if (ex1.getTypeData() != ex2.getTypeData()) {
				if (ex1.getTypeData() == program.getIntType() && ex2.getTypeData() == program.getStringType()) {
					Cast cast = new Cast(ex1.getLineNumber(), "String", ex1);
					cast.setTypeData(program.getStringType());
					eqCmp.setE1(cast);
				} else if (ex1.getTypeData() == program.getStringType() && ex2.getTypeData() == program.getIntType()) {
					Cast cast = new Cast(ex2.getLineNumber(), "String", ex2);
					cast.setTypeData(program.getStringType());
					eqCmp.setE2(cast);
				} else if ((ex1.getTypeData() instanceof LCPLClass) && (ex2.getTypeData() instanceof LCPLClass)) {
					Cast cast1 = new Cast(ex1.getLineNumber(), "Object", ex1);
					cast1.setTypeData(program.getObjectType());
					
					Cast cast2 = new Cast(ex2.getLineNumber(), "Object", ex2);
					cast2.setTypeData(program.getObjectType());
					
					eqCmp.setE1(cast1);
					eqCmp.setE2(cast2);
				} else if ((ex1.getTypeData() instanceof LCPLClass) && ex2.getTypeData() == program.getNullType()) {
					Cast cast = new Cast(ex1.getLineNumber(), "Object", ex1);
					cast.setTypeData(program.getObjectType());
					eqCmp.setE1(cast);
				} else if ((ex2.getTypeData() instanceof LCPLClass) && ex1.getTypeData() == program.getNullType()) {
					Cast cast = new Cast(ex2.getLineNumber(), "Object", ex2);
					cast.setTypeData(program.getObjectType());
					eqCmp.setE2(cast);
				} else {
					throw new LCPLException(expr.getLineNumber(), "Invalid type of parameters for == expression");
				}
			}
			
			expr.setTypeData(program.getIntType());
		} else if (expr instanceof Subtraction ||
					expr instanceof Multiplication ||
					expr instanceof Division ||
					expr instanceof LessThan ||
					expr instanceof LessThanEqual) {
			BinaryOp binaryOp = (BinaryOp)expr;
			Expression ex1 = binaryOp.getE1();
			Expression ex2 = binaryOp.getE2();
			
			if (ex1 == null || ex2 == null) {
				throw new LCPLException(expr.getLineNumber(), "Cannot convert a value of type (none) into Int");
			}
			
			setExprData(ex1, varsInScope);
			setExprData(ex2, varsInScope);
			
			if (ex1.getTypeData() != program.getIntType()) {
				throw new LCPLException(expr.getLineNumber(), "Cannot convert a value of type " + ex2.getType() +
																" into Int");
			}
			if (ex2.getTypeData() != program.getIntType()) {
				throw new LCPLException(expr.getLineNumber(), "Cannot convert a value of type " + ex2.getType() +
																" into Int");
			}
			
			expr.setTypeData(program.getIntType());
		} else if (expr instanceof IfStatement) {
			IfStatement ifStatement = (IfStatement)expr;
			Expression cond = ifStatement.getCondition();
			Expression thenBranch = ifStatement.getIfExpr();
			Expression elseBranch = ifStatement.getThenExpr();
			Type thenType, elseType;
			
			if (cond == null || thenBranch == null) {
				throw new ExpressionInvalidTypesException(expr.getLineNumber(), "if",
															"Int, any type, maybe any type");
			}
			
			setExprData(cond, varsInScope);
			setExprData(thenBranch, varsInScope);
			setExprData(elseBranch, varsInScope);
			
			if (cond.getTypeData() != program.getIntType()) {
				throw new ExpressionInvalidTypesException(expr.getLineNumber(), "if",
						"Int, any type, maybe any type");
			}
			
			thenType = thenBranch.getTypeData();
			elseType = elseBranch == null ? program.getNoType() : elseBranch.getTypeData();
			
			if (thenType == program.getNoType() || elseType == program.getNoType()) {
				expr.setTypeData(program.getNoType());
			} else if (thenType == elseType) {
				expr.setTypeData(thenType);
			} else if (thenType == program.getNullType()) {
				expr.setTypeData(elseType);
			} else if (elseType == program.getNullType()) {
				expr.setTypeData(thenType);
			} else if ((thenType instanceof LCPLClass) && (elseType instanceof LCPLClass)) {
				expr.setTypeData(getLastCommonAncestor((LCPLClass)thenType, (LCPLClass)elseType));
			} else {
				expr.setTypeData(program.getNoType());
			}
		} else if (expr instanceof WhileStatement) {
			WhileStatement whileStatement = (WhileStatement)expr;
			Expression cond = whileStatement.getCondition();
			Expression body = whileStatement.getLoopBody();
			
			if (cond == null) {
				throw new LCPLException(expr.getLineNumber(), "If condition must be Int");
			}
			
			setExprData(cond, varsInScope);
			setExprData(body, varsInScope);
			
			if(cond.getTypeData() != program.getIntType()) {
				throw new LCPLException(expr.getLineNumber(), "If condition must be Int");
			}
			
			expr.setTypeData(program.getNoType());
		} else if (expr instanceof Block) {
			Block block = (Block)expr;
			List<Expression> expressions = block.getExpressions();
			
			if (expressions == null || expressions.size() == 0) {
				expr.setTypeData(program.getNoType());
			} else {
				for (Expression ex : expressions) {
					setExprData(ex, varsInScope);
				}
				expr.setTypeData(expressions.get(expressions.size() - 1).getTypeData());
			}
		} else if (expr instanceof LocalDefinition) {
			LocalDefinition localDef = (LocalDefinition)expr;
			Type varType = getTypeByName(localDef.getType());
			Expression init = localDef.getInit();
			Expression scope = localDef.getScope();
			
			if (varType == null) {
				throw new LCPLException(expr.getLineNumber(), "Class " + localDef.getType() + " not found.");
			}
			localDef.setVariableType(varType);
			if (init != null) {
				setExprData(init, varsInScope);
				if (init.getTypeData() != varType) {
					if (validCast(varType, init.getTypeData())) {
						Cast cast = new Cast(init.getLineNumber(), varType.getName(), init);
						cast.setTypeData(varType);
						localDef.setInit(cast);
					} else {
						throw new LCPLException(expr.getLineNumber(), "Cannot convert a value of type " +
																init.getType() + " into " + localDef.getType());
					}
				}
			}
			if (scope == null) {
				expr.setTypeData(program.getNoType());
			} else {
				List<Variable> newVarsInScope = new ArrayList<Variable>();
				for (Variable var : varsInScope) {
					if (!(var instanceof LocalDefinition)) {
						newVarsInScope.add(var);
					} else {
						LocalDefinition localVar = (LocalDefinition)var;
						if (!localVar.getName().equals(localDef.getName())) {
							newVarsInScope.add(var);
						}
					}
				}
				newVarsInScope.add(localDef);
				setExprData(scope, newVarsInScope);
				expr.setTypeData(scope.getTypeData());
			}
		} else if (expr instanceof Cast) {
			Cast cast = (Cast)expr;
			Expression ex = cast.getE1();
			Type type = getTypeByName(cast.getType());
			
			if (ex == null) {
				throw new ExpressionInvalidTypesException(expr.getLineNumber(), "cast", "type name, any type");
			}
			
			if (type == null) {
				throw new LCPLException(expr.getLineNumber(), "Class " + cast.getType() + " not found.");
			}
			
			setExprData(ex, varsInScope);
			expr.setTypeData(type);
		} else if (expr instanceof SubString) {
			SubString subStr = (SubString)expr;
			Expression string = subStr.getStringExpr();
			Expression start = subStr.getStartPosition();
			Expression end = subStr.getEndPosition();
			
			if (string == null || start == null || end == null) {
				throw new ExpressionInvalidTypesException(expr.getLineNumber(), "substring", "String, Int, Int");
			}
			
			setExprData(string, varsInScope);
			setExprData(start, varsInScope);
			setExprData(end, varsInScope);
			
			if (string.getTypeData() != program.getStringType() || start.getTypeData() != program.getIntType() ||
				end.getTypeData() != program.getIntType()) {
				throw new ExpressionInvalidTypesException(expr.getLineNumber(), "substring", "String, Int, Int");
			}
			
			expr.setTypeData(program.getStringType());
		} else if (expr instanceof Assignment) {
			Assignment asg = (Assignment)expr;
			Expression value = asg.getE1();
			Variable foundVar = getVarByName(varsInScope, asg.getSymbol());
			Type varType;
			
			if (foundVar == null) {
				throw new LCPLException(expr.getLineNumber(), "Attribute " + asg.getSymbol().replace("self.", "") +
																" not found in class " +
																((FormalParam)getVarByName(varsInScope, "self")).getType());
			}
			asg.setSymbolData(foundVar);
			varType = getVarType(foundVar);
			
			if (value == null) {
				throw new ExpressionInvalidTypesException(expr.getLineNumber(), "assignment", "symbol, any type");
			}
			
			setExprData(value, varsInScope);
			
			if (value.getTypeData() != varType) {
				if (validCast(varType, value.getTypeData())) {
					Cast cast = new Cast(value.getLineNumber(), varType.getName(), value);
					cast.setTypeData(varType);
					asg.setE1(cast);
				} else {
					throw new LCPLException(expr.getLineNumber(), "Cannot convert a value of type " +
															value.getType() + " into " + varType.getName());
					
				}
			}
			
			expr.setTypeData(varType);
		} else if (expr instanceof NewObject) {
			NewObject newObj = (NewObject)expr;
			Type type = getTypeByName(newObj.getType());
			
			if (type == null) {
				throw new LCPLException(expr.getLineNumber(), "Class " + newObj.getType() + " not found.");
			} else if (!(type instanceof LCPLClass)) {
				throw new LCPLException(expr.getLineNumber(), "Illegal instruction : new " + newObj.getType());
			}
			
			expr.setTypeData(type);
		} else if (expr instanceof StaticDispatch) {
			StaticDispatch stDispatch = (StaticDispatch)expr;
			Expression ex = stDispatch.getObject();
			Type type = getTypeByName(stDispatch.getType());
			boolean found = false;
			List<Method> methods;
			
			if (ex == null) {
				throw new ExpressionInvalidTypesException(expr.getLineNumber(), "static dispatch",
															"object.calss.method, params");
			}
			
			if (type == null || !(type instanceof LCPLClass)) {
				throw new LCPLException(expr.getLineNumber(), "Class " + stDispatch.getType() + " not found.");
			}
			
			setExprData(ex, varsInScope);
			
			if (!(ex.getTypeData() instanceof LCPLClass)) {
				throw new ExpressionInvalidTypesException(expr.getLineNumber(), "static dispatch",
															"object.class.method, params");
			}
			
			if (!(validCast(type, ex.getTypeData()))) {
				throw new LCPLException(expr.getLineNumber(), "Cannot convert from " + ex.getType() + " to " +
										type.getName() + " in StaticDispatch");
			}
			
			stDispatch.setSelfType(type);
			
			methods = allFeatures.get(type.getName()).getLeft();
			
			for (Method mtd : methods) {
				if (mtd.getName().equals(stDispatch.getName())) {
					stDispatch.setMethod(mtd);
					found = true;
					break;
				}
			}
			
			if (!found) {
				throw new LCPLException(expr.getLineNumber(), "Method " + stDispatch.getName() +
																" not found in class " + stDispatch.getType());
			}
			
			computeDispatch(stDispatch, varsInScope);
		} else if (expr instanceof Dispatch) {
			Dispatch dispatch = (Dispatch)expr;
			Expression ex = dispatch.getObject();
			Boolean found = false;
			List<Method> methods = null;
			
			if (ex == null) {
				ex = new Symbol(expr.getLineNumber(), "self");
				dispatch.setObject(ex);
			}

			setExprData(ex, varsInScope);
			
			if (!(ex.getTypeData() instanceof LCPLClass)) {
				throw new ExpressionInvalidTypesException(expr.getLineNumber(), "dispatch", "object.method, params");
			}
			
			methods = allFeatures.get(ex.getType()).getLeft();
				
			for (Method mtd : methods) {
				if (mtd.getName().equals(dispatch.getName())) {
					dispatch.setMethod(mtd);
					found = true;
					break;
				}
			}
			
			if (!found) {
				throw new LCPLException(expr.getLineNumber(), "Method " + dispatch.getName() + " not found in class " + 
																ex.getType());
			}
			
			computeDispatch(dispatch, varsInScope);
		}
	}
}
