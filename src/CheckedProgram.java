import ro.pub.cs.lcpl.*;

public class CheckedProgram extends ExpressionChecker {
	public CheckedProgram(Program p) throws Exception {
		super(p);
		
		p.getClasses().add(program.getObjectType());
		p.getClasses().add(program.getIoType());
		p.getClasses().add(program.getStringType());
	}
}
