import ro.pub.cs.lcpl.*;

public class SetFeatureTypesAndSelf extends HasMain {
	public SetFeatureTypesAndSelf(Program p) throws Exception {
		super(p);
		
		if (program.getClasses() != null) {
			for (LCPLClass cls : program.getClasses()) {
				if (cls.getFeatures() != null) {
					for (Feature feature : cls.getFeatures()) {
						FormalParam self = new FormalParam("self", "");
						self.setVariableType(cls);
						
						if (feature instanceof Method) {
							Method mtd = (Method)feature;
							String typeName = mtd.getReturnType();
							Type type = getTypeByName(typeName);
							
							self.setType(cls.getName());
							
							if (type == null) {
								throw new LCPLException(mtd.getLineNumber(), "Class " + typeName + " not found.");
							}
							mtd.setReturnTypeData(type);
							
							mtd.setParent(cls);
							
							mtd.setSelf(self);
							if (mtd.getParameters() != null) {
								for (FormalParam fp : mtd.getParameters()) {
									String fpTypeName = fp.getType();
									Type fpType = getTypeByName(fpTypeName);
									
									if (fpType == null || fpType == program.getNoType()) {
										throw new LCPLException(fp.getLineNumber(), "Class " + fpTypeName + 
																					" not found.");
									}
									fp.setVariableType(fpType);
								}
							}
						} else if (feature instanceof Attribute) {
							Attribute atr = (Attribute)feature;
							String typeName = atr.getType();
							Type type = getTypeByName(typeName);
							
							if (type == null || type == program.getNoType()) {
								throw new LCPLException(atr.getLineNumber(), "Class " + typeName + " not found.");
							}
							
							atr.setTypeData(type);
							
							if (atr.getInit() != null) {
								self.setType(type.getName());
								atr.setAttrInitSelf(self);
							}
						}
					}
				}
			}
		}
	}
}
